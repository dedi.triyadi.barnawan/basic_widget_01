import 'package:flutter/material.dart';

class ColumnDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      body: SafeArea(
        child: Center(
          child: Card(
            elevation: 12,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(32.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  FlutterLogo(size: 48),
                  SizedBox(
                    height: 16,
                  ),
                  Column(
                    children: [
                      Text(
                        'Title of The Card',
                        style: Theme.of(context).textTheme.headline5,
                      ),
                      Text(
                        'Some description',
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class RowDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      body: SafeArea(
        child: Center(
          child: Card(
            elevation: 12,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 24.0,
                vertical: 36,
              ),
              child: Row(
                children: [
                  FlutterLogo(size: 48),
                  SizedBox(height: 16),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          'Title of the Card',
                          style: Theme.of(context).textTheme.headline5,
                        ),
                        Text(
                          'Some description',
                          style: Theme.of(context).textTheme.headline6,
                        ),
                      ],
                    ),
                  ),
                  Icon(
                    Icons.navigate_next,
                    size: 36,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class StackDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      body: SafeArea(
        child: Center(
          child: Container(
              margin: EdgeInsets.all(16),
              child: Stack(
                children: [
                  Opacity(
                    opacity: 1,
                    child: Card(
                      elevation: 12,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(24),
                      ),
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 24.0, vertical: 36),
                        child: Row(
                          children: [
                            FlutterLogo(size: 48),
                            SizedBox(width: 16),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(
                                    'Title of the Card',
                                    style:
                                        Theme.of(context).textTheme.headline5,
                                  ),
                                  Text(
                                    'Some description',
                                    style:
                                        Theme.of(context).textTheme.headline6,
                                  ),
                                ],
                              ),
                            ),
                            Icon(
                              Icons.navigate_next,
                              size: 36,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                      left: 8,
                      top: 8,
                      child: Icon(
                        Icons.star,
                        color: Colors.amber,
                        size: 36,
                      ))
                ],
              )),
        ),
      ),
    );
  }
}

class ListDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView.builder(
          itemCount: 10,
          itemBuilder: (context, index) {
            return Container(
              child: Stack(
                children: [
                  Card(
                    elevation: 12,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24),
                    ),
                    color: Colors.white,
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 24.0, vertical: 36),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(24),
                        gradient: LinearGradient(
                            colors: [Color(0xFF213B6C), Color(0xFF0059A5)]),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          FlutterLogo(size: 48),
                          SizedBox(
                            width: 16,
                          ),
                          Expanded(
                            child: Column(
                              children: [
                                Text(
                                  'Title of the Card',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline5
                                      .copyWith(color: Colors.white),
                                ),
                                Text('Some discription',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headline6
                                        .copyWith(color: Colors.white)),
                              ],
                            ),
                          ),
                          Icon(
                            Icons.navigate_next,
                            size: 36,
                            color: Colors.white,
                          )
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                      left: 8,
                      top: 8,
                      child: Icon(
                        Icons.star,
                        size: 36,
                        color: Colors.deepOrangeAccent,
                      ))
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}

class App01 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue,
      body: SafeArea(
        child: Card(
          elevation: 12,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
              width: 200,
              height: 100,
            ),
          ),
        ),
      ),
    );
  }
}

class App02 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(children: [
        Container(
          color: Colors.red,
          child: Center(
            child: Text(
              'Halaman-1',
            ),
          ),
        ),
        Container(
          color: Colors.green,
          child: Center(
            child: Text(
              'Halaman-2',
            ),
          ),
        ),
        Container(
          color: Colors.blue,
          child: Center(
              child: Text(
            'Halaman-3',
          )),
        ),
      ]),
    );
  }
}

class App03 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [TextField()],
      ),
    );
  }
}
